from datetime import datetime, timedelta
from math import isnan
from time import time
import sys
import argparse
sys.path.insert(0,'../../')
from pyndfd import ndfd

parser = argparse.ArgumentParser()
parser.add_argument("variable")
args = parser.parse_args()


# game farm rd
lat = 42.449167
lon = 283.55966

# variable to get
var = 'qpf'

# specify a specific area/grid to use
area = 'conus'

startTime = int(time() * 1000)

filename = ndfd.getVariable(args.variable,area)

print("Reading %s" %(filename[0]))

analysis = ndfd.getLocationData(filename[0], lat, lon)

print(analysis['variables'])

#ndfd.plotData(analysis['variables'],'t2m')

print('\nElapsed: ' + str(int(time() * 1000) - startTime))
