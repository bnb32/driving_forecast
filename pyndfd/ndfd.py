# Copyright (c) 2015 Marty Sullivan
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

'''

	NDFD Forecast Retrieval Routines

	Author: 	Marty J. Sullivan and Brandon N. Benton
	Purpose:	Routines that will cache NDFD forecast variables locally
			to allow for easy and fast forecast analysis by lat/lon

'''

###########
#         #
# IMPORTS #
#         #
###########

from datetime import datetime, timedelta
from getpass import getuser
from math import isnan, sqrt
from .ndfd_defs import ndfdDefs
import numpy as np
from numpy.ma.core import MaskedConstant as NAN
from os import makedirs, path
from pyproj import Geod, Proj
from shutil import rmtree
import shutil
from sys import stderr
from tempfile import gettempdir
from urllib.request import urlretrieve, urlopen
import json
import cfgrib
import xarray as xr
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import pygrib

#############
#           #
# CONSTANTS #
#           #
#############

DEFS = ndfdDefs()
G = Geod(ellps='clrk66')

CACHE_SERVER_BUFFER_MIN = 15

NDFD_LOCAL_SERVER = None
NDFD_REMOTE_SERVER = 'http://tgftp.nws.noaa.gov/SL.us008001/ST.opnl/DF.gr2/'
NDFD_DIR = 'DC.ndfd' + path.sep + 'AR.{0}' + path.sep + 'VP.{1}' + path.sep
NDFD_STATIC = 'static' + path.sep + 'DC.ndfd' + path.sep + 'AR.{0}' + path.sep
NDFD_VAR = 'ds.{0}.bin'
NDFD_TMP = gettempdir() + path.sep + str(getuser()) + '_pyndfd' + path.sep

########################
#                      #
# FUNCTION DEFINITIONS #
#                      #
########################

'''
  Function:	setLocalCacheServer
  Purpose:	Set a server to use instead of weather.noaa.gov
  Params:
	uri:	String denoting the server URI to use
'''

def setLocalCacheServerNDFD(uri):
    global NDFD_LOCAL_SERVER 
    NDFD_LOCAL_SERVER = uri


'''
  Function: 	getLatestForecastTime
  Purpose:  	For caching purposes, compare this time to cached time to see if
		the cached variable needs to be updated
'''

def getLatestForecastTime():
    latestTime = datetime.utcnow()
    if latestTime.minute <= CACHE_SERVER_BUFFER_MIN:
        latestTime = (datetime.utcnow() - timedelta(hours=1))
    return latestTime.replace(minute=0, second=0, microsecond=0)

'''
  Function:	getVariable
  Purpose:	Cache the requested variable if not already cached and return
		the paths of the cached files
  Params:
	var:	The NDFD variable to retrieve
	area:	The NDFD grid area to retrieve
'''

def getVariable(var, area):
    gribs = []
    dirTime = NDFD_TMP + getLatestForecastTime().strftime('%Y-%m-%d-%H') + path.sep
    
    if not path.isdir(dirTime):
        try: rmtree(NDFD_TMP)
        except: pass
        makedirs(dirTime)
    if area in DEFS['vars']:
        for vp in DEFS['vars'][area]:
            if var in DEFS['vars'][area][vp]:
                varDir = NDFD_DIR.format(area, vp)
                varName = varDir + NDFD_VAR.format(var)
                localDir = dirTime + varDir
                localVar = dirTime + varName
                
                if not path.isdir(localDir):
                    makedirs(localDir)
                if not path.isfile(localVar):
                    if NDFD_LOCAL_SERVER != None:
                        remoteVar = NDFD_LOCAL_SERVER + varName
                        urlretrieve(remoteVar, localVar)
                    else:
                        remoteVar = NDFD_REMOTE_SERVER + varName
                        urlretrieve(remoteVar, localVar)
                if not path.isfile(localVar):
                    raise RuntimeError('Cannot retrieve NDFD variables at this time. Try again in a moment.')
                gribs.append(localVar)
    else:
        raise ValueError('Invalid Area: ' + str(area))

    return gribs

'''
  Function:	getElevationVariable
  Purpose:	Cache the static elevation variable if not already 
                cached and return the path of the cached file
  Params:
	area:	The NDFD grid area to retrieve elevation for
  
  Notes:
	- Cannot be retrieved from weather.noaa.gov, must use a local cache server
	  using the format in const NDFD_STATIC
	- Puerto Rico terrian info not currently available. 
	- Terrain data for NDFD will be updated sometime in 2015
'''

def getElevationVariable(area):
    if area == 'puertori':
        raise ValueError('Elevation currently not available for Puerto Rico. Set elev=False')
    if NDFD_LOCAL_SERVER == None:
        raise RuntimeError('Local cache server must provide elevation data. Specify cache server with ndfd.setLocalCacheServer(uri)')
    if not path.isdir(NDFD_TMP):
        makedirs(NDFD_TMP)
    remoteVar = NDFD_LOCAL_SERVER + NDFD_STATIC.format(area) + NDFD_VAR.format('elev')
    localDir = NDFD_TMP + NDFD_STATIC.format(area)
    localVar = localDir + NDFD_VAR.format('elev')
    if not path.isdir(localDir):
        makedirs(localDir)
    if not path.isfile(localVar):
        urlretrieve(remoteVar, localVar)
    if not path.isfile(localVar):
        raise RuntimeError('Cannot retrieve NDFD variables at this time. Try again in a moment.')
    return localVar

'''
  Function:	getSmallestGrid
  Purpose:	Use the provided lat, lon coordinates to find the smallest
		NDFD area that contains those coordinates. Return the name of the area.
  Params:
	lat:	Latitude 
	lon:	Longitude
'''

def getSmallestGrid(lat, lon):
    smallest = 'neast'
    minDist = G.inv(lon, lat, DEFS['grids'][smallest]['lonC'], DEFS['grids'][smallest]['latC'])[-1]
    for area in DEFS['grids'].keys():
        if area == 'conus' or area == 'nhemi' or area == 'npacocn':
            continue
        curArea = DEFS['grids'][area]
        smallArea = DEFS['grids'][smallest]
        dist = G.inv(lon, lat, curArea['lonC'], curArea['latC'])[-1]
        if dist < minDist:
            minDist = dist
            smallest = area

    return smallest

'''
  Function:	getNearestGridPoint
  Purpose:	Find the nearest grid point to the provided coordinates. 
                Return the indexes to the numpy array as well as the lat/lon 
                and grid coordinates of the grid point.
  Params:
	ds:		    xarray data for grib file
	lat:		Latitude
	lon:		Longitude
'''

def getNearestGridPoint(ds,lat,lon):
    
    lat_tmp = 180. + lat if lat < 0. else lat
    lon_tmp = 360. + lon if lon < 0. else lon

    lats = ds['lats']
    lons = ds['lons']
    lat_arr = np.array(lats.data)-np.float64(lat_tmp)
    lon_arr = np.array(lons.data)-np.float64(lon_tmp)
    
    dist = (lat_arr)**2+(lon_arr)**2
    idy, idx = np.where(dist == dist.min())
    
    gLat = float(lats.data[idy, idx])
    gLon = float(lons.data[idy, idx])
    
    gLat = gLat - 180. if gLat > 90. else gLat
    gLon = gLon - 360. if gLon > 180. else gLon
    
    return idx, idy, gLat, gLon      

'''
  Function:	validateArguments
  Purpose:	Validate the arguments passed into an analysis function to make sure
		they will work with each other.
  Params:
	var:		The NDFD variable being requested
	area:		The NDFD grid area being requested
	timeStep:	The time step to be used in the returned analysis
	minTime:	The minimum forecast time to analyze
	maxTime:	The maximum forecast time to analyze
  Notes:
	- maxTime is not currently being evaluated
'''

def validateArguments(var, area, timeStep, minTime, maxTime):
    if timeStep < 1:
        raise ValueError('timeStep must be >= 1')
    if minTime != None and minTime < getLatestForecastTime():
        raise ValueError('minTime is before the current forecast time.')
    if maxTime != None and maxTime < getLatestForecastTime():
        raise ValueError('maxTime is before the current forecast time.')
 
    try:
        areaVP = DEFS['vars'][area]
    except IndexError:
        raise ValueError('Invalid Area.')

    validVar = False
    for vp in areaVP:
        if var in areaVP[vp]:
            validVar = True
            break
    if not validVar:
        raise ValueError('Variable not available in area: ' + area)

'''
  Function:	getRegionData
  Purpose:	To get raw data over an area for 
                ndfd forecast period
'''
def getRegionData(ds_path):

    ds = xr.open_dataset(ds_path,engine='cfgrib')#, backend_kwargs={'filter_by_keys': {'stepUnits': 0}})

    analysis = {}
    analysis['variables'] = { }
        
    analysis['lats'] = ds.variables['latitude']
    analysis['lons'] = ds.variables['longitude']
    analysis['steps'] = ds.get('step').values

    for variable in ds.data_vars:
        varData = {}
        analysis['variables'][variable] = {}
        
        if 'step' not in ds.variables[variable].dims:
            analysis['times'] = None
            varData[0] = ds.variables[variable][:,:].values
        else:
            analysis['times'] = ds.valid_time.values.astype('datetime64[s]').tolist()
            varData = {i: ds.variables[variable][i,:,:].values for i,t in enumerate(analysis['times'])}
    
        analysis['variables'][variable]['data'] = varData
        analysis['variables'][variable]['units'] = ds.variables[variable].attrs.get('GRIB_units')
        analysis['variables'][variable]['standard_name'] = ds.variables[variable].attrs.get('GRIB_cfName')
        analysis['variables'][variable]['long_name'] = ds.variables[variable].attrs.get('GRIB_name')
        analysis['variables'][variable]['missing_value'] = ds.variables[variable].attrs.get('GRIB_missingValue')  

        tmp = [datetime.fromisoformat(str(x)) for x in analysis['times']]
    return analysis


'''
  Function:	getLocationData
  Purpose:	To get raw data at a specific location for 
                ndfd forecast period
'''


def getLocationData(ds_path, lat, lon):
    
    analysis = {}
    analysis['variables'] = { }
    
    ds = getRegionData(ds_path)

    x,y,gLat,gLon = getNearestGridPoint(ds,lat,lon)

    analysis['nearest_lon'] = gLon
    analysis['nearest_lat'] = gLat

    for variable in ds['variables']:
        varData = {}
        analysis['variables'][variable] = {}

        for t in ds['variables'][variable]['data']:
            varData[t] = float(ds['variables'][variable]['data'][t][y,x])
        analysis['variables'][variable]['data'] = varData 
        analysis['variables'][variable]['units'] = ds['variables'][variable]['units']
        analysis['variables'][variable]['standard_name'] = ds['variables'][variable]['standard_name']
        analysis['variables'][variable]['long_name'] = ds['variables'][variable]['long_name']
        analysis['variables'][variable]['missing_value'] = ds['variables'][variable]['missing_value']

    return analysis


'''
def getLocationData(ds_path, lat, lon):
    
    analysis = {}
    analysis['variables'] = { }
    
    ds = getRegionData(ds_path)

    x,y,gLat,gLon = getNearestGridPoint(ds,lat,lon)

    analysis['nearest_lon'] = gLon
    analysis['nearest_lat'] = gLat
    
    tmp = ds.isel(y=y,x=x)
    
    for variable in tmp.data_vars:
        varData = {}
        
        if 'step' not in tmp.variables[variable].dims:
            value = float(tmp.variables[variable][0,0].values)
            varData['Time Average'] = value

        else:
            times = ds.valid_time.values.astype('datetime64[s]').tolist()
            varData = {t.isoformat(): float(tmp.variables[variable][i,0,0].values) for i,t in enumerate(times)}
    
        
        analysis['variables'][variable] = varData
        analysis['variables'][variable]['units'] = ds.variables[variable].attrs.get('GRIB_units')
        analysis['variables'][variable]['standard_name'] = ds.variables[variable].attrs.get('GRIB_cfName')
        analysis['variables'][variable]['long_name'] = ds.variables[variable].attrs.get('GRIB_name')
        analysis['variables'][variable]['missing_value'] = ds.variables[variable].attrs.get('GRIB_missingValue')

    return analysis
'''

'''
  Function:	getLocationWeather
  Purpose:	To get weather at a specific location for 
                ndfd forecast period
'''

def getLocationWeather(ds_path, lat, lon):
    
    analysis = {}
    analysis['variables'] = { }
    
    ds = getRegionData(ds_path)

    x,y,gLat,gLon = getNearestGridPoint(ds,lat,lon)

    analysis['nearest_lon'] = gLon
    analysis['nearest_lat'] = gLat
    
    tmp = ds.isel(y=y,x=x)
    
    for variable in tmp.data_vars:
        varData = {}

        if 'step' not in tmp.variables[variable].dims:
            value = float(tmp.variables[variable][0,0].values)
            varData['Time Average'] = value

        else:
            times = ds.valid_time.values.astype('datetime64[s]').tolist()
            varData = {t.isoformat(): parseWeatherString(local_use_section[int(tmp.variables[variable][i,0,0].values)]) for i,t in enumerate(times)}
    
        
        analysis['variables'][variable] = varData
        analysis['variables'][variable]['units'] = ds.variables[variable].attrs.get('GRIB_units')
        analysis['variables'][variable]['standard_name'] = ds.variables[variable].attrs.get('GRIB_cfName')
        analysis['variables'][variable]['long_name'] = ds.variables[variable].attrs.get('GRIB_name')
        analysis['variables'][variable]['missing_value'] = ds.variables[variable].attrs.get('GRIB_missingValue')

    return analysis


'''
  Function:	plotData
  Purpose:      Plot data from getLocationData
'''

def plotData(data,var):

    xtmp = [mdates.date2num(datetime.strptime(x,'%Y-%m-%d:%H')) for x in data[var].keys()]
    x = sorted(xtmp)
    ytmp = [y for y in data[var].values()]
    y = []

    #hack for removing weird temp values
    for i in range(len(ytmp)):
        if ytmp[i]<0:
            ytmp[i] = (ytmp[i-1]+ytmp[i+1])/2

    for i in range(len(ytmp)):
        idx = xtmp.index(x[i])
        y.append(ytmp[idx])
    
    
    fig = plt.figure()
    ax = fig.add_subplot(111)

    ax.xaxis.set_minor_locator(mdates.HourLocator(interval=3))
    ax.xaxis.set_major_locator(mdates.HourLocator(interval=12))
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d:%H'))

    for i in range(len(y)):
        if y[i] == ytmp[0]: color = 'red'
        else: color = 'blue'

        plt.plot_date(x[i], y[i], color = color)
    
    plt.plot(x,y)
    plt.ylim([min(y)-np.std(y), max(y)+np.std(y)])
    plt.xticks(rotation=90)
    fig.autofmt_xdate()

    fig_name = NDFD_TMP + path.sep + '%s.png' %(var)
    fig.savefig(fig_name)

    print('Saved %s' %(fig_name))

    return fig

'''

  Function:	parseWeatherString
  Purpose:	To create a readable, English string describing the weather
  Params:
	wxString:	The weather string to translate into English
  Notes:
	- See http://graphical.weather.gov/docs/grib_design.html for details

'''
def parseWeatherString(wxString):
    weatherString = ''
    visibility = float('nan')

    words = wxString.split('^')
    for word in words:
        entries = word.split(':')
        coverage = entries[0]
        weather = entries[1]
        intensity = entries[2]
        vis = entries[3]
        attributes = entries[4].split(',')

        ws = ''
        prepend = False
        OR = False
        likely = False

        if '<NoCov>' in coverage:
            pass
        elif 'Lkly' in coverage:
            likely = True
        elif coverage in DEFS['wx']['coverage']:
            ws += DEFS['wx']['coverage'][coverage] + ' '
        else:
            stderr.write('WARNING: Unknown coverage code: ' + coverage + '\n'); stderr.flush()

        if '<NoInten>' in intensity:
            pass
        elif intensity in DEFS['wx']['intensity']:
            ws += DEFS['wx']['intensity'][intensity] + ' '
        else:
            stderr.write('WARNING: Unknown intensity code: ' + intensity + '\n'); stderr.flush()

        if '<NoWx>' in weather:
            pass
        elif weather in DEFS['wx']['weather']:
            ws += DEFS['wx']['weather'][weather] + ' '
        else:
            stderr.write('WARNING: Unknown weather code: ' + weather + '\n'); stderr.flush()

        if likely:
            ws += 'likely '

        for attribute in attributes:
            if len(attribute) == 0 or '<None>' in attribute or 'Mention' in attribute:
                pass
            elif attribute == 'Primary':
                prepend = True
            elif attribute == 'OR':
                OR = True
            elif attribute in DEFS['wx']['hazards']:
                ws += 'with ' + DEFS['wx']['hazards'][attribute] + ' '
            elif attribute in DEFS['wx']['attributes']:
                ws += DEFS['wx']['attributes'][attribute] + ' '
            else:
                stderr.write('WARNING: Unknown attribute code: ' + attribute + '\n'); stderr.flush()

        if len(weatherString) == 0:
            weatherString = ws
        else:
            if prepend and OR:
                weatherString = ws + 'or ' + weatherString.lower()
            elif prepend:
                weatherString = ws + 'and ' + weatherString.lower()
            elif OR:
                weatherString += 'or ' + ws.lower()
            else:
                weatherString += 'and ' + ws.lower()

        if '<NoVis>' in vis:
            vis = float('nan')
        elif vis in DEFS['wx']['visibility']:
            vis = DEFS['wx']['visibility'][vis]
        else:
            stderr.write('WARNING: Unknown visibility code: ' + vis + '\n'); stderr.flush()
            vis = float('nan')

        if not isnan(vis) and isnan(visibility):
            visibility = vis
        elif not isnan(vis) and vis < visibility:
            visibility = vis

    if len(weatherString) == 0:
        weatherString = '<NoWx>'
    else:
        weatherString = weatherString.strip().capitalize()

    return weatherString, visibility

'''
  Function:	parseAdvisoryString
  Purpose:	To create a readable, English string describing current weather hazards
  Params:
	wwaString:	The Watch, Warning, Advisory string to translate to English
  Notes:
	- See http://graphical.weather.gov/docs/grib_design.html for details
'''

def parseAdvisoryString(wwaString):
    advisoryString = ''

    words = wwaString.split('^')
    for word in words:
        if '<None>' in word:
            continue

        entries = word.split('.')
        hazard = entries[0]
        advisory = entries[1]

        if hazard in DEFS['wwa']['hazards']:
            advisoryString += DEFS['wwa']['hazards'][hazard] + ' '
        else:
            stderr.write('WARNING: Unknown hazard code: ' + hazard + '\n'); stderr.flush()

        if advisory in DEFS['wwa']['advisories']:
            advisoryString += DEFS['wwa']['advisories'][advisory] + '\n'
        else:
            stderr.write('WARNING: Unknown advisory code: ' + advisory + '\n'); stderr.flush()

    if len(advisoryString) == 0:
        advisoryString = '<None>'
    else:
        advisoryString = advisoryString.strip().title()

    return advisoryString

'''

  Function:	unpackString
  Purpose:	To unpack the packed binary string in the local use section of NDFD gribs
  Params:
	raw:	The raw byte string containing the packed data
'''

def unpackString(raw):
    num_bytes, remainder = divmod(len(raw) * 8 - 1, 7)

    #i = int(raw.encode('hex'), 16)
    i = int(raw.encode(), 16)
    if remainder:
        i >>= remainder

    msg = []
    for _ in range(num_bytes):
        byte = i & 127
        if not byte:
            msg.append(ord("\n"))
        elif 32 <= byte <= 126:
            msg.append(byte)
        i >>= 7
    msg.reverse()
    msg = b"".join(chr(c) for c in msg)

    codes = []
    for line in msg.splitlines():
        if len(line) >= 4 and (line.count(':') >= 4 or line.count('.') >= 1 or '<None>' in line):
            codes.append(line)

    return codes


