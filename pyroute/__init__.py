#driving forecast
# Copyright (c) 2021 Brandon N. Benton
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

'''

	Weather based driving forecast routines

	Author: 	Brandon N. Benton
	Purpose:	Routines that will predict travel time based on weather conditions 

'''
###########
#         #
# IMPORTS #
#         #
###########

from datetime import datetime, timedelta
import numpy as np
import json

#############
#           #
# CONSTANTS #
#           #
#############

CACHE_SERVER_BUFFER_MIN = 20

class Weather:
    pass

class DriveRoute:
    conditions = {
        'precip' : { 
            'light': {
                'meters' : [0, 0.0025], 
                'speed reduction' : [0.02, 0.08]},
            'moderate' : {
                'meters' : [0.0025, 0.01], 
                'speed reduction' : [0.08, 0.13]},
            'heavy' : {
                'meters' : [0.01, 0.05], 
                'speed reduction' : [0.13, 0.40]},
            'violent' : {
                'meters' : [0.05, 1.0], 
                'speed reduction' : [0.40, 0.95]}
            }
        }
        
    def __init__(self,ds,route):
        rds = self.get_route_data(route)

        self.ds = ds
        self.lats = rds['lats']
        self.lons = rds['lons']
        self.dists = rds['dists']

        self.ds_times = [datetime.fromisoformat(str(x)) for x in ds['times']]

        #assuming times is in seconds
        self.drive_times = rds['times']

        self.adjusted_times = self.drive_times.copy()
        
        self.speeds = rds['speeds']
        
        self.start_time = self.ds_times[0]

    def get_route_data(self,filename):
        data = json.load(open(filename))
        route = {}

        route['coords'] = data['routes'][0]['geometry']['coordinates']
        
        route['lats'] = [x[1] for x in route['coords']]
        route['lons'] = [x[0] for x in route['coords']]
        
        route['speeds'] = []
        route['dists'] = []
        
        for i in range(len(data['routes'][0]['legs'])):
            route['speeds']+=data['routes'][0]['legs'][i]['annotation']['speed']
            route['dists']+=data['routes'][0]['legs'][i]['annotation']['distance']

        route['times'] = [d/v for d,v in zip(route['dists'],route['speeds'])]

        return route

    def get_nearest_grid_point(self,lat,lon):
        lat_tmp = 180. + lat if lat < 0. else lat
        lon_tmp = 360. + lon if lon < 0. else lon

        lats = self.ds['lats']
        lons = self.ds['lons']
        lat_arr = np.array(lats.data)-np.float64(lat_tmp)
        lon_arr = np.array(lons.data)-np.float64(lon_tmp)
        
        dist = (lat_arr)**2+(lon_arr)**2
        idy, idx = np.where(dist == dist.min())
        
        gLat = float(lats.data[idy, idx])
        gLon = float(lons.data[idy, idx])
        
        gLat = gLat - 180. if gLat > 90. else gLat
        gLon = gLon - 360. if gLon > 180. else gLon
        
        return idx, idy, gLat, gLon 

    def get_time_bounds(self,time):
        if time<self.ds_times[0]:
            return 0,0,self.ds_times[0],self.ds_times[0]
        if time>self.ds_times[-1]:
            return len(self.ds_times)-1,len(self.ds_times)-1,self.ds_times[-1],self.ds_times[-1] 

        for i,t in enumerate(self.ds_times):
            if self.ds_times[i-1]<time<self.ds_times[i]:
                return i-1,i,self.ds_times[i-1],self.ds_times[i]
            if self.ds_times[i]==time:
                return i,i,self.ds_times[i],self.ds_times[i]
           
    def time_interpolate_weather(self,lat,lon,time):
        x, y, gLat, gLon = self.get_nearest_grid_point(lat,lon)
        iLow, iHigh, tLow, tHigh = self.get_time_bounds(time)

        weather = {}
        
        for var in self.ds['variables']:
            if tLow == tHigh:
                weather[var] = float(self.ds['variables'][var]['data'][iLow][y,x])
            else:
                time_interval = (tHigh-tLow).total_seconds()
                alpha = (time-tLow).total_seconds()/time_interval
                weather[var] = float((1-alpha)*self.ds['variables'][var]['data'][iLow][y,x] + alpha*self.ds['variables'][var]['data'][iHigh][y,x])
        
        return weather

    def get_weather(self,lat,lon,time):  
        weather = {}
        
        data = self.time_interpolate_weather(lat,lon,time)

        #precip in meters
        #divide by density of water in kg/m^3
        weather['precip'] = data['paramId_0']/(997.0)

        #ice accumulation in meters
        weather['ice'] = 0

        #visibility in meters
        weather['visibility'] = 1000

        return weather

    def get_speed_reduction(self,weather):
        for w in self.conditions:
            for c in self.conditions[w]:
                min_amount = self.conditions[w][c]['meters'][0]
                max_amount = self.conditions[w][c]['meters'][1]

                if min_amount < weather[w] < max_amount:
                    alpha = weather[w]/(max_amount - min_amount)
                    min_reduction = self.conditions[w][c]['speed reduction'][0]
                    max_reduction = self.conditions[w][c]['speed reduction'][1]
                    
                    val = min_reduction*(1.0-alpha)+max_reduction*alpha
                    return val
        return 0        
      
    def get_adjusted_time(self,i,t):
        weather = self.get_weather(self.lats[i],self.lons[i],t)
        adjusted_time = self.drive_times[i]/(1.0-self.get_speed_reduction(weather))
        return timedelta(seconds=adjusted_time)

    def get_drive_time(self):
        time = self.ds_times[0]
        for i in range(len(self.dists)):
            val = self.get_adjusted_time(i,time)
            self.adjusted_times[i] = val
            time+=val 
        return time-self.ds_times[0]

