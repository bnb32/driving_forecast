import json
import pyroute
from pyndfd import ndfd

ds_path = ndfd.getVariable('qpf','conus')
rds_path = './data/ith-syr.json'

ds = ndfd.getRegionData(ds_path[0])
drive_route = pyroute.DriveRoute(ds,rds_path)

print(drive_route.get_drive_time())
